-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 09 2022 г., 21:04
-- Версия сервера: 10.4.24-MariaDB
-- Версия PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `status`
--

CREATE TABLE `status` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `static_content` varchar(1000) NOT NULL,
  `razmetka` varchar(1000) NOT NULL,
  `dynamic_content` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `status`
--

INSERT INTO `status` (`id`, `user_id`, `static_content`, `razmetka`, `dynamic_content`) VALUES
(1, 1, '{\"first_section\":\"true\",\"second_section\":\"true\",\"third_section\":\"true\",\"fourth_section\":\"true\",\"fifth_section\":\"true\"}', '{\"razmetka_first\":\"true\",\"razmetka_second\":\"true\"}', '{\"dynamic_first\":\"true\",\"dynamic_second\":\"true\",\"dynamic_third\":\"false\",\"dynamic_fourth\":\"false\"}'),
(2, 3, '{\"first_section\":\"true\",\"second_section\":\"true\",\"third_section\":\"true\",\"fourth_section\":\"true\",\"fifth_section\":\"false\"}', '{\"razmetka_first\":\"true\",\"razmetka_second\":\"false\"}', '{\"dynamic_first\":\"true\",\"dynamic_second\":\"true\",\"dynamic_third\":\"false\",\"dynamic_fourth\":\"true\"}'),
(3, 4, '{\"first_section\":\"true\",\"second_section\":\"false\",\"third_section\":\"true\",\"fourth_section\":\"false\",\"fifth_section\":\"true\"}', '{\"razmetka_first\":\"false\",\"razmetka_second\":\"true\"}', '{\"dynamic_first\":\"true\",\"dynamic_second\":\"false\",\"dynamic_third\":\"true\",\"dynamic_fourth\":\"true\"}');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `surname` text CHARACTER SET utf8 NOT NULL,
  `patronymic` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(15) CHARACTER SET utf8 NOT NULL,
  `password` varchar(110) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `patronymic`, `email`, `password`) VALUES
(1, 'Татьяна', 'Иванова', 'Михайловна', 'ivanova@mail.ru', ''),
(3, 'Иван', 'Иванов', 'Иванович', 'ivanova@mail.ru', ''),
(4, 'Петр', 'Петрович', 'Петрович', 'ivanova@mail.ru', ''),
(25, 'Анна', 'Лифанова', 'Владимировна', 'pashintsevaa@gm', '1234');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
