<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\DB;


class AccountController extends Controller
{

    public function logIn()
    {
        if (!empty($_POST) && ($_SERVER["REQUEST_METHOD"] == "POST")) {
            $email = $this->test_input($_POST["email"]);
            $password = $this->test_input($_POST["password"]);

            if ($email && $password) {
                $this->view->location('/index');
            }
        }
        $this->view->render();
    }
    public function register()
    {
        if (!empty($_POST) && ($_SERVER["REQUEST_METHOD"] == "POST")) {

                $name = $this->test_input($_POST["name"]);
                $surname = $this->test_input($_POST["surname"]);
                $patronymic = $this->test_input($_POST["patronymic"]);
                $email = $this->test_input($_POST["email"]);
                $password = $this->test_input($_POST["password"]);
            $newUser = [
                'name' => $name,
                'surname' => $surname,
                'patronymic' => $patronymic,
                'email' => $email,
                'password' => $password,

            ];
             $uniqUser = $this->uniqueUser($name, $surname, $email);
        debug($uniqUser);
         if (!$uniqUser) {
            $user = $this->addUser($newUser);
        
        } else {
            echo "User already exists";
        }
        }
       
        
        $this->view->render();
    }

    public function addUser($newUser)
    {
        return $this->db->insertUser('users', $newUser);
    }
    public function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


    function uniqueUser($name, $surname, $email) {
        $unique_email = $this->db->query('SELECT * FROM `users`  WHERE 
                                        `email` = "' . $email . '" AND 
                                        `name` = "' . $name . '" AND 
                                        `surname` = "' . $surname . '"');
        return $unique_email;
    }




}
