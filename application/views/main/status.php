<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="max-width=1280, initial-scale=1.0">
    <script  src="./public/js/jquery-3.6.0.min.js"></script>
    <script  src="./public/js/liteChart.min.js"></script>
    <script  src="./public/js/slick.min.js"></script>

    <script  src="./application/scripts/form.js"></script>
    <link href="https://allfont.ru/allfont.css?fonts=micra" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="./public/styles/slick.css">
    <link rel="stylesheet" href="./public/styles/bootstrap.css">
    <link rel="stylesheet" href="./public/styles/style.css">
    <link rel="stylesheet" href="./public/styles/cabinet.css">
    <link rel="stylesheet" href="./public/styles/1440.css">
    <link rel="stylesheet" href="./public/styles/checkbox.css">

    <title>ЦТС УПП</title>
</head>
<body>
    <div class="title">
        <h2>Статус прохождения обучения</h2>
       
    </div>
    <main>


        <table class="table table-bordered">
            <!--  -->
            <thead>
                <tr valign="center">
                <td scope="col" rowspan="3" >ФИО обучающегося</td>
                <td scope="col" rowspan="1" colspan="5" class="light-background">Статический контент</td>
                <td scope="col" colspan="2" class="light-background">Разметка</td>
                <td scope="col" colspan="4"class="light-background" >Динамический контент</td>
                </tr>
                <tr class="blue-background">
                    <!-- 1 -->
                    <td>09.02.2022</td>
                    <td>09.02.2022</td>
                    <td>09.02.2022</td>
                    <td>09.02.2022</td> 
                    <td>09.02.2022</td> 
                    <!-- 22 -->
                    <td>09.02.2022</td> 
                    <td>09.02.2022</td> 
                    <!-- 3 -->
                    <td>09.02.2022</td> 
                    <td>09.02.2022</td> 
                    <td>09.02.2022</td> 
                    <td>09.02.2022</td> 

                </tr>
                <tr>
                    <!-- 1 -->
                    <td>Задания 1-4</td>
                    <td>Задания 4-7</td>
                    <td>Задания 8</td>
                    <td>Задания 9-10</td>
                    <td>Задания 11-12</td>
                    <!-- 2 -->
                    <td>Тест по разметке</td>
                    <td>Разметка реальных сессий</td>
                    <!-- 3 -->
                    <td>Задания 1-4</td>
                    <td>Задания 4-7</td>
                    <td>Задания 8</td>
                    <td>Задания 9-10</td>
                </tr>
            </thead>
            <!--  -->
            
            <tbody>
            <?php foreach ($data as  $user) : 
            
                $status = json_decode($user['static_content'], TRUE);
                ?>
                <tr valign="center">
                    
                    <th scope="row" valign="center">
                        <?= $user['surname']?> 
                        <?= $user['name']?>
                        <?=$user['patronymic'] ?>
                    </th>
                    <!-- 1 --> 
                    <?php foreach ($status as $name => $val) : ?>
                        <td class="no-padding">       
                            <label class="checkbox-other"> 
                                <input type="checkbox" name =<?=$name?>  id = <?= $user['id'] ?>
                                <?php if ($val === 'true') : ?> checked<?php endif; ?>/>
                                <span></span>
                            </label>
                        </td>
                        <?php endforeach; ?>
                    
                    <!-- 2 -->
                    <?php $status = json_decode($user['razmetka'], TRUE);?>
                    <?php foreach ($status as $name => $val) : ?>

                        <td class="no-padding">           
                            <label class="checkbox-other">
                                <input type="checkbox" name=<?=$name?>   id = <?= $user['id'] ?>
                                <?php if ($val === 'true') : ?> checked<?php endif; ?>/>
                                <span></span>
                            </label>
                        </td>
                        <?php endforeach; ?>
                    <!-- 3 -->
                    <?php $status = json_decode($user['dynamic_content'], TRUE);?>
                    <?php foreach ($status as $name => $val) : ?>

                        <td class="no-padding">           
                            <label class="checkbox-other">
                                <input type="checkbox" name=<?=$name?>   id = <?= $user['id'] ?>
                                <?php if ($val ==='true') : ?> checked<?php endif; ?>/>
                                <span></span>
                            </label>
                
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        
        </table>
 
    </main>
    <footer>
        <p>Разработка UPPlabcommunity2021</p>
    </footer>
</body>
<!-- <script  src="/js/liteChart.min.js"></script> -->

</html>