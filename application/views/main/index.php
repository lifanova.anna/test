<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1280, initial-scale=1.0">
    <script  src="./public/js/jquery-3.6.0.min.js"></script>
    <script  src="./public/js/slick.min.js"></script>
    <script  src="./public/js/app.js"></script>
    <link href="https://allfont.ru/allfont.css?fonts=micra" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/public/styles/slick.css">
    <link rel="stylesheet" href="/public/styles/bootstrap.css">
    <link rel="stylesheet" href="/public/styles/style.css">
    <link rel="stylesheet" href="/public/styles/1440.css">
    <title>ЦТС УПП</title>
</head>
<body>
    <header>
        <div class="logo">
            <a href="#">ЦТС <span>УПП</span></a>
        </div>
        <!-- <div class="container"> -->
            <div class="menu">
                <a class="menu-btn btn" href="#">Контакты</a>
                <a class="menu-btn btn"  href="#"> Направления</a>
                <a class="menu-btn btn"  href="#">Группы сопровождения</a>
                <a class="menu-btn btn active-menu "  href="#">Обучение</a>
            </div>
        <!-- </div> -->

        <div class="lk">
            <div class="circle">
                <a class="avatar" href="cabinet"></a>
            </div>
            <div class="name"><?=$surname ?> <?=substr($name, 0, 2) ?>. <?=mb_substr($patronymic,0 ,1)?>. </div>
        </div>
        
    </header>
    <main>
        <h1>Обучение</h1>
        <div class="container learning">
            <div class="row">
                <div class="learning-menu">АСУ СТ ТСТ</div>
                <div class="learning-menu">АСУ СТ ЦТТ</div>
                <div class="learning-menu">ГИД</div>
                <div class="learning-menu">АИП</div>
                <div class="learning-menu learning-menu-active">ИИ</div>
                <div class="learning-menu">РПА</div>
                <div class="learning-menu">Общее</div>
            </div>
        </div>
        <div class="container flex">
            <div class="col-8 ii">
                <div class="prev-btn">
                    <svg viewBox="0 0 60 70" width="50" height="30">
                        <polyline
                            points="0,40 30,0 60,40 0,40"
                            style="fill:#fff;
                            stroke:#fff;
                            stroke-width:2">
                        </polyline>
                    </svg>
                </div>

                <div class="video-items">
                    <div class="video-item-line">
                        <div class="col-5 video-item">
                            <iframe src="https://www.youtube.com/embed/t93dj6Ldiow"></iframe>
                            <div class="video-title">Как стать разработчиком за 1 день</div>
                        </div>
                        <div class="col-5 video-item">
                            <iframe src="https://www.youtube.com/embed/RzS494arw78"></iframe>
                            <div class="video-title">Как стать разработчиком за 1 день</div>
                        </div>
                    </div>
                    <div class="video-item-line">
                        <div class="col-5 video-item">
                            <iframe src="https://www.youtube.com/embed/t93dj6Ldiow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="video-title">Как стать разработчиком за 1 день</div>
                        </div>
                        <div class="col-5 video-item">
                            <iframe src="https://www.youtube.com/embed/RzS494arw78" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="video-title">Как стать разработчиком за 1 день</div>
                        </div>
                    </div>
                    <div class="video-item-line">
                        <div class="col-5 video-item">
                            <iframe src="https://www.youtube.com/embed/t93dj6Ldiow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="video-title">Как стать разработчиком за 1 день</div>
                        </div>
                        <div class="col-5 video-item">
                            <iframe src="https://www.youtube.com/embed/RzS494arw78" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="video-title">Как стать разработчиком за 1 день</div>
                        </div>
                    </div>
                </div>

                <div class="next-btn">
                    <svg viewBox="0 0 60 70" width="50" height="30">
                        <polyline
                            points="0,40 30,0 60,40 0,40"
                            style="fill:#fff;
                            stroke:#fff;
                            stroke-width:2">
                        </polyline>
                    </svg>
                </div>
            </div>
            <div class="col-5 poster-block">
                <h3>Предстоящие мероприятия</h3>
                <div class="poster-list">
                <ol> 
                    <li>
                        <div class="dots"></div>
                        <span class="date"> 22.10 9-00 МСК</span>
                        <p>Обучение по работе с платформой ChatNavigator</p>
                    </li>
                    <li>
                        <div class="dots"></div>
                        <span class="date"> 23.10 9-00 МСК</span>
                        <p>Обучение по работе с платформой ChatNavigator</p>
                    </li>
                    <li>
                        <div class="dots"></div>
                        <span class="date"> 24.10 9-00 МСК</span>
                        <p>Обучение по работе с платформой ChatNavigator</p>
                    </li>     
                    <li>
                        <div class="dots active-dots"></div>
                        <span class="date"> 25.10 9-00 МСК</span>
                        <p>Обучение по работе с платформой ChatNavigator</p>
                    </li>
                </оl>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <p>Разработка UPPlabcommunity2021</p>
    </footer>
</body>
</html>