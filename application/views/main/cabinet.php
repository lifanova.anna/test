<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="max-width=1280, initial-scale=1.0">
    <script  src="./public/js/jquery-3.6.0.min.js"></script>
    <script  src="./public/js/liteChart.min.js"></script>
    <script  src="./public/js/slick.min.js"></script>

    <script  src="./public/js/app.js"></script>
    <link href="https://allfont.ru/allfont.css?fonts=micra" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="./public/styles/slick.css">
    <link rel="stylesheet" href="./public/styles/bootstrap.css">
    <link rel="stylesheet" href="./public/styles/style.css">
    <link rel="stylesheet" href="./public/styles/cabinet.css">
    <link rel="stylesheet" href="./public/styles/1440.css">
    <title>ЦТС УПП</title>
</head>
<body>
    <div class="title">
        <h2>Личная карточка специалиста ЦТС УПП</h2>
       
    </div>
    <main>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-3 person">
                    <div class="person">
                        <div class="name">Иванова Татьяна Михайловна</div>
                        
                        <div class="frame"></div><div class="photo"></div>
                    </div>
                </div>
                <div class="col-9">
                    <div class="navigation">
                        <div class="btn-nav">Пройти обучение по ЦГ</div>
                        <div class="btn-nav">Подать предложение по созданию ЦС</div>
                        <div class="btn-nav">Оставить пожелание</div>
                    </div>
                    <div class="info">
                        <ul>
                            <li>ЦТС: УПП</li>
                            <li>Отдел: АСУ Д</li>
                            <li>ИВЦ: Московский</li>
                            <li>Сопровождаемая ИС: АСУ СТ ТСТ</li>
                            <li>Рабочие группы: АСУ-СТ-ТСТ-МСК, АСУ-СТ-ТСТ-ЭКС-Д</li>
                            <li>Статус: Эксперт</li>
                        </ul>
                    </div>
                    <div class="info">
                        <p>Выполненные объекты ЕСПП за текущий месяц</p>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Запросы</th>
                                    <th scope="col">Наряды по запросу</th>
                                    <th scope="col">Изменения</th>
                                    <th scope="col">Наряды по изменению</th>
                                    <th scope="col">Проекты</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td > 120 </td>
                                    <td> 240</td>
                                    <td> 5 </td>
                                    <td> 40 </td>
                                    <td> 3 </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-5">
                    <div class="learning-status">
                        <p>Текущий статус прохождения обучения по цифровой грамотности:</p>
                        <div class="progress">
                            <div class="progress-ii">
                                <div class="steps-items">
                                    <div class="steps-item started">step 1</div>
                                    <div class="steps-item">step 2</div>
                                    <div class="steps-item">step 3</div>
                                    <div class="steps-item">step 4</div>
                                </div>
                                <span>ИИ</span>
                            </div>
                            <div class="progress-rpa">
                                <div class="steps-items">
                                    <div class="steps-item started success">step 1</div>
                                    <div class="steps-item started ">step 2</div>
                                    <div class="steps-item">step 3</div>
                                    <div class="steps-item">step 4</div>
                                </div>
                                <span>RPA</span>
                            </div>
                        </div>
                        <div class="status">
                            Уровень цифровой грамотности:<br/>
                            новичок
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <div class="statistics">
                        <div class="select-cs">
                            <div class="select-btn">Выбрать ЦС <div class="arrow"></div></div>
                            <div class="option-btn">Чат-бот классификатор ошибок АСОУП</div>
                            <div class="option-btn">Чат-бот администратор по ИС АиП</div>
                        </div>
                        <div class="graph">
                        <div class="title">Статистика использования ЦС</div>
                        <div class="flex">
                            <div class="chart" id="Fchart"></div>
                            <div class="chart-name">
                                <span>Чат-бот 1</span>
                                <span>Чат-бот 2</span>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                </div>
               
            </div>
        </div>
    </main>
    <footer>
        <p>Разработка UPPlabcommunity2021</p>
    </footer>
</body>
<!-- <script  src="/js/liteChart.min.js"></script> -->

</html>