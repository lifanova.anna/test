<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="max-width=1280, initial-scale=1.0">
    <script src="./public/js/jquery-3.6.0.min.js"></script>
    <script src="./public/js/liteChart.min.js"></script>
    <script src="./public/js/slick.min.js"></script>
    <script src="./application/scripts/form.js"></script>

    <script src="./public/js/app.js"></script>
    <link href="https://allfont.ru/allfont.css?fonts=micra" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="./public/styles/slick.css">
    <link rel="stylesheet" href="./public/styles/bootstrap.css">
    <link rel="stylesheet" href="./public/styles/style.css">
    <link rel="stylesheet" href="./public/styles/cabinet.css">
    <link rel="stylesheet" href="./public/styles/checkbox.css">
    <link rel="stylesheet" href="./public/styles/1440.css">
    <title>Вход</title>
</head>

<body>

    <main class="container-fluid row justify-content-center">
        <div class="title">
            <h2>Вход</h2>
        </div>
        <form class="col-3 flex flex-column justify-content-center">
            <div class="form-group">
                <label for="Email">Email</label>
                <input type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder="Введите email">
            </div>
            <div class="form-group">
                <label for="Password">Пароль</label>
                <input type="password" class="form-control" id="Password" placeholder="Password">
            </div>
            <div class="buttons">
                <button type="submit" class="btn btn-primary">Войти</button>
                <a href="/register" class="btn btn-primary">Регистрация</a>
            </div>

        </form>
    </main>
    <footer>
        <p>Разработка UPPlabcommunity2021</p>
    </footer>
</body>
<!-- <script  src="/js/liteChart.min.js"></script> -->

</html>