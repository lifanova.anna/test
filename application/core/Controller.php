<?php
namespace application\core;
use application\lib\DB;

abstract class Controller {

    public $params;
    public $view;
    public $db;

    public function __construct($params) {
        $this->model = $this->loadModel($params['controller']); 
        $this->db = new DB;

        $this->params = $params;
        // $this->checkAcl();
        $this->view = new View($this->params);
        $this->model = $this->loadModel($params['controller']); 
        
    }

    public function loadModel($name) {
        $path = 'application\models\\' . ucfirst($name) . 'Model';
		if (class_exists($path)) {
			return new $path;
		}
        
    }
 
    public function checkAcl() {
        $acl = require 'application/acl/'.$this->params['controller'].'.php';
        debug($acl);
    }

}