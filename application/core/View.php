<?php
namespace application\core;
use application\controllers;

class View  {

    public $params;
    public $layout = 'default';
    
    public function __construct($params) {
        $this->params = $params;
        $this->path = $params['controller'].'/'.$params['action'];
    }

    public function render($data=[]) {
        extract($data);
		$path = 'application/views/'.$this->path.'.php';
		if (file_exists($path)) {
			ob_start();
			require $path;
			$content = ob_get_clean();
			require 'application/views/layouts/'.$this->layout.'.php';
		}
    }
    function location($url) {
        header('Location: '.$url);
        exit;        
    }
}