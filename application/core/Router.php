<?php
namespace application\core;
// use application\controllers;


class Router {
        
    protected $routes = [];

    protected $params = [];

    function __construct() {
        $arr = require 'application/config/routes.php'; 
        foreach ($arr as $route => $params){
            $this->addRoute($route, $params);
        }
    }

    function addRoute($route, $params) {
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;
    }
/* Проверка существования маршрута */
    function match() {
        $url = trim($_SERVER['REQUEST_URI'],'/');
        // debug($_SERVER);
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    function run() {
        if( $this->match() ) {
           $path = 'application\controllers\\'.ucfirst($this->params['controller']).'Controller';
            if ($path){
                $action = $this->params['action'];
                if(method_exists($path, $action)) {
                    $controller = new $path($this->params);
                    $controller->$action();
                } else {
                    echo  $path .'  \\' . $action() ;
                    
                }
            } else { 
                echo 'Не найден'. $path;
            }
        }else{
            echo "Маршрут не найден";
        };
        
    }
}