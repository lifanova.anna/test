<?php
namespace application\core;
// use application\models\MainModel;
use application\lib\DB;

abstract class Model {

    public $db;

    public function __construct() {
            $this->db = new DB;
    }
    public function getAllUsers() {
        $data = $this->db->row('SELECT * FROM users');
        return $data;
    }
    public function getUser() {
        $data = $this->db->row('SELECT * FROM users WHERE id = 1');
        return $data;
    }
    public function getStatus($user_id) {
        $status = $this->db->row('SELECT static_content, razmetka, dynamic_content FROM status WHERE user_id = '.$user_id.'');
        return $status;
    }

    public function getStatusByUserId($user_id) {
        $status = $this->db->row('SELECT * FROM status WHERE user_id = '.$user_id.'');
        return $status;
    }
    public function getDate() {
        $date_now = date("d.m.y");
        return $date_now;
    } 
    public function getTime() {
        $time_now = date("H:i:s");
        echo($time_now);
        return $time_now;
    }



    
}