<?php
namespace application\lib;
use PDO;

class DB {

    protected $db;

    function __construct() {

            $config = require 'application/config/db.php';
            $this->db = new PDO("mysql:host=" . $config['host'] . ";dbname=" . $config['dbname'], $config['user'], $config['password']);
       
    }

    public function query($sql) {
        $query = $this->db->query($sql);
        debug($sql);
        return $query;
    }

    public function row($sql){
        $result = $this->query($sql);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function column($sql){
        $result = $this->query($sql);
        return $result->fetchColumn();
    }

    // public function select($what,$from,$where = null,$order = null)
    // {   $fetched = array();         
    //     $sql = 'SELECT '.$what.' FROM '.$from;  
    //     if($where != null) $sql .= ' WHERE '.$where;  
    //     if($order != null) $sql .= ' ORDER BY '.$order;  
        
    //     $query = $this->query($sql);  
    //     if($query) {  
    //         $rows = $this->query($query);  
    //         for($i = 0; $i < $rows; $i++) {  
    //             $results = mysqli_fetch_assoc($query); 
    //             $key = array_keys($results);
    //             $numKeys = count($key);
    //             for($x = 0; $x < $numKeys; $x++) { 
    //                 $fetched[$i][$key[$x]] = $results[$key[$x]];                           
    //             }                                          
    //         } 
    //         return $fetched; 
    //     } else  {  
    //         return false;  
    //     }                   
    // }

    
    public function insertUser($table,$values,$rows = null)  {  
        $insert = 'INSERT INTO '.$table;  
        if($rows != null) {  
            $insert .= ' ('.$rows.')';  
        }  
        $values = implode('","',$values); 
        $insert .= '( `id`, `name`, `surname`, `patronymic`, `email`, `password`) VALUES (NULL,"'.$values.'")';  
        $ins = $this->query($insert);
        return ($ins) ? true : false;
 
    }
}