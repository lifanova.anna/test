<?php

return [
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],
    'logIn' => [
        'controller' => 'account',
        'action' => 'logIn',
    ], 
    'register' => [
        'controller' => 'account',
        'action' => 'register',
    ], 
    'application/controllers/AccountController/register' => [
        'controller' => 'account',
        'action' => 'register',
    ],
    'application/controllers/AccountController/logIn' => [
        'controller' => 'account',
        'action' => 'logIn',
    ],
    'cabinet' => [
        'controller' => 'main',
        'action' => 'cabinet',
    ],
    'index' => [
        'controller' => 'main',
        'action' => 'index',
    ],
    'status' => [
        'controller' => 'main',
        'action' => 'status',
    ],   
    'application/controllers/MainController/UpdateStatus' => [
        'controller' => 'main',
        'action' => 'UpdateStatus',
    ],
];