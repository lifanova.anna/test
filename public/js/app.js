$('document').ready(function() {
    $(function(){
        $('.video-items').slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 2,
            // autoplay: true,
            prevArrow: '.prev-btn',
            nextArrow: '.next-btn'
        });
    });

    $(".select-btn").click(function() {
      $(".option-btn").slideToggle("slow");
      $('.arrow').toggleClass('rotate')
    });

    if ($('.success')){
        $('.success').append('<div class="done">&#10004;</div>');
    }

    $('input:checkbox').on('change', function() {
        $.ajax({
            url: 'application/core/Controller/UpdateStatus',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: {
                user_id :  this.id,
                name : this.name,
                checked : this.checked,
            

            },
            success: function(data){
                // alert(data);
                
                
            }
        });
    });

});






// Chart
$(document).ready(function(){
	// Create liteChart.js Object
	settings = {};
	let d = new liteChart("chart", settings);

	// Set labels
	d.setLabels(["0", "0", "1", "2", "2"]);

	// Set legends and values
	d.addLegend({"ChB-1": "Day",
                "stroke": "#CDDC39",
                "fill": "#CDDC39", 
                "values": [0, 0, 1, 2, 2]
            });
	d.addLegend({"ChB-2": "Night", 
                "stroke": "#3b95f7", 
                "fill": "#3b95f7", 
                "values": [0, 0 , 2]
            });
	d.addLegend({"ChB-3": "Night", 
                "stroke": "#be514a", 
                "fill": "#be514a", 
                "values": [0, 1, 2, 3 ]
            });

	// Inject chart into DOM object
	let div = document.getElementById("Fchart");
	d.inject(div);

	// Draw
	d.draw();
});

